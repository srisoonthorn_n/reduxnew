import React, { useEffect } from 'react'
import UserToolBar from '../components/UserToolBar'
import UserList from '../components/UserList'
import { useDispatch } from 'react-redux'
import { fetchUsers } from '../action/userAction'
import Header from './Header'

const User = () => {

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(fetchUsers())
    }, [])

    return (
        <div>
            <Header />
            <center>
                <h1>User</h1>
                <UserToolBar />
                <UserList />
            </center>
        </div>
    )
}
export default User