import React from 'react'
import { Menu, Icon } from 'antd';

class Header extends React.Component {

  handleClick = e => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  };

  render() {
    return (
      <Menu onClick={this.handleClick} mode="horizontal" theme="dark">
           <Menu.Item key="alipay">
           <a href={"/users"} rel="noopener noreferrer" ><Icon type="home" />Home</a>
        </Menu.Item>
        <Menu.Item>
        <a href={"/processLogout"}  rel="noopener noreferrer"><Icon type="logout" />Logout</a>
        </Menu.Item>
      </Menu>
    );
  }
}

export default Header