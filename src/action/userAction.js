export const SEARCH_USERS = 'SEARCH_USERS'
export const FETCH_USER_BEGIN = 'FETCH_USER_BEGIN'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_ERROR = 'FETCH_USER_ERROR'

export const searchUsers = (value) => {
    return {
        type: SEARCH_USERS,
        payLoad: value
    }
}

export const fetchUsers = () => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch('https://jsonplaceholder.typicode.com/users/')
            .then(res => res.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
            })
            .catch(error => dispatch(fetchUserError(error)))
    }
}

export const fetchUserBegin = () => {
    return {
        type: FETCH_USER_BEGIN,
    }
}

export const fetchUserSuccess = users => {
    return {
        type: FETCH_USER_SUCCESS,
        payLoad: users
    }
}
export const fetchUserError = error => {
    return {
        type: FETCH_USER_ERROR,
        payLoad: error
    }
}