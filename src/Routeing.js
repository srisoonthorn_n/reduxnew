import React from 'react';
import './App.css';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import User from './page/User';
import ToDopage from './page/ToDopage';
import Login from './page/Login';
import PrivateRoute from './components/PrivateRoute';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import Header from './page/Header';

const Routeing = () => {
    return (

        <BrowserRouter>
            <Route path="/processLogin" render={() => {
                localStorage.setItem("isLogin", true);
                return <Redirect to="/users" />
            }}></Route>

            <Route path="/processLogout" render={() => {
                localStorage.setItem("isLogin", false);
                return <Redirect to="/" />
            }}></Route>
            {/* 
            <Route path="/" component={Header} /> */}
            <Route path="/" component={Login} exact={true} />
            <PrivateRoute path="/users" component={User} exact={true} />
            <Route path="/todo/:user_id" component={ToDopage} />
        </BrowserRouter>
    )
}
export default Routeing