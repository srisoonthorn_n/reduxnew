import React from 'react'
import { useDispatch } from 'react-redux'
import { searchUsers } from '../action/userAction'

const UserToolBar = () => {
    const dispatch = useDispatch()

    const onSeachUser = (evnet) => {
        let textValue = evnet.target.value
        dispatch(searchUsers(textValue))

    }
    return (
        <div>
            <div>
                <input onChange={onSeachUser}></input>

            </div>

        </div>
    )
}
export default UserToolBar