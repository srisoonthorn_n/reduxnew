import React from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

const UserList = () => {
    const userList = useSelector(state => state.UserCompState.users)
    const userSearch = useSelector(state => state.UserCompState.searchUsers)

    const history = useHistory();

    const Onsearch = () => {
        if (userSearch == '') {
            return userList
        } else {
            return userList.filter((value) => {
                return value.name.match(userSearch)
            })
        }
    }
    const OnClickTodo = (index) => {
        history.push("/todo/" + index)
    }
    return (
        <div>
            <h2>User List</h2>
            <center><div>
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>

                        </tr>
                    </thead>
                    <tbody>
                        {
                            Onsearch().map((item) => {
                                return (
                                    <tr>
                                        <td>{item.id}</td>
                                        <td>{item.name}</td>
                                        <td>{item.email}</td>
                                        <td><button onClick={() => OnClickTodo(item.id)}>Todo</button></td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
            </center>
        </div>
    )
}
export default UserList
