import { todoReducer } from "./TodoReducer";
import { combineReducers } from "redux";
import { UserReducer } from "./UserReducer";

export const rootReducer = combineReducers({
    todoCompState: todoReducer,
    UserCompState: UserReducer,
})