import { SEARCH_TODO, FILTER_TODO, FETCH_TODOS_BEGIN, FETCH_TODOS_SUCCESS, FETCH_TODOS_ERROR, DONE_AND_DOING } from "../action/todoAction"

const initialState = {
    searchText: '',
    filterType: 'All',
    data: []
}

export const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEARCH_TODO:
            return {
                ...state,
                searchText: action.payLoad
            }
        case FILTER_TODO:
            return {
                ...state,
                filterType: action.payLoad
            }
        case FETCH_TODOS_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_TODOS_SUCCESS:
            return {
                ...state,
                data: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_TODOS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
            case DONE_AND_DOING :
                const newData = [...state.data]
                ///action payLoad
                newData.forEach(value=> {
                    if(action.payLoad == value.id){
                        value.completed = true
                    }
                })
                return{
                    ...state,
                    data :newData
                }
        default:
            return state
    }
}


// export const todoFilter = (state = initialState, action) => {
//     switch (action.type) {
//         case FILTER_TODO:
//             return {
//                 ...state,
//                 filterType: action.payLoad
//             }
//         default:
//             return state
//     }
// }
