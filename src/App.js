import React from 'react';
import logo from './logo.svg';
import './App.css';
import ToDopage from './page/ToDopage';
import User from './page/User';

function App() {
  return (
    <div className="App">
      <User />
      <ToDopage />
    </div>
  );
}

export default App;
